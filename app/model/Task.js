/*
 * File: app.js
 * @autor Andreas Deitmer
 * @version 1.0
 */
Ext.define('TodosApp.model.Task', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'id'
            },
            {
                name: 'name'
            },
            {
                name: 'done',
                type: 'boolean'
            }
        ],
        proxy: {
            type: 'rest',
            url: '/tasks',
            format: 'json'
        }
    }
});