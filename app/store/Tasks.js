/*
 * File: app.js
 * @autor Andreas Deitmer
 * @version 1.0
 */

Ext.define('TodosApp.store.Tasks', {
    extend: 'Ext.data.Store',
    requires: [
        'TodosApp.model.Task'
    ],

    config: {
        autoLoad: true,
        model: 'TodosApp.model.Task',
        storeId: 'Tasks'
    }
});