/*
 * File: app.js
 * @autor Andreas Deitmer
 * @version 1.0
 */
Ext.define('TodosApp.view.MainPanel', {
    extend: 'Ext.Panel',

    config: {
        id: 'mainPanel',
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'textfield',
                id: 'taskTextField',
                itemId: 'mytextfield',
                padding: '0 0 0 10',
                label: '',
                placeHolder: 'What needs to be done?'
            },
            {
                xtype: 'checkboxfield',
                id: 'markAllCheckbox',
                itemId: 'mycheckbox',
                label: 'Mark all tasks done',
                labelAlign: 'right',
                labelWidth: '80%'
            },
            {
                xtype: 'list',
                id: 'taskList',
                itemId: 'mylist',
                layout: {
                    type: 'vbox'
                },
                itemTpl: [
                    '<div class="view"><input type="checkbox" <tpl  if="done">checked</tpl> />&nbsp;<label <tpl  if="done">class="done"</tpl> >{name}</label><a class="destroy"></a></div><input class="edit" type="text" value="{name}">'
                ],
                store: 'Tasks',
                flex: 1
            },
            {
                xtype: 'label',
                html: '<b>?</.b> items left',
                id: 'storeInfo',
                padding: '0 0 0 15'
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                title: 'Todos'
            }
        ]
    }

});