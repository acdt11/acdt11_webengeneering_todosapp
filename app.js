/*
 * File: app.js
 * @autor Andreas Deitmer
 * @version 1.0
 */

Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    models: [
        'Task'
    ],
    stores: [
        'Tasks'
    ],
    views: [
        'MainPanel'
    ],
    name: 'TodosApp',
    controllers: [
        'MainController'
    ],

    launch: function() {

        Ext.create('TodosApp.view.MainPanel', {fullscreen: true});
    }

});
